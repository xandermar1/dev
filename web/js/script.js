// window.location.protocol = “http:”
// window.location.host = “domain.com”
// window.location.pathname = “/path/page.html”
// window.location.search = “?s = variable”

function header_image(img){
    var ret = '';
    img ? ret = `<div class="text-center mb-3"><img class="img-fluid" src="` + data.header_image + `"></div>` : false;
    return ret;
}

function getIndiv(response) {

    $.each(response, function(key, data) {

        $('#xanDisplay').html('<div class="container"></div>');

        // set title
        $('title').text('DanGibson.me Blog | '+data.title);

        var individual = `
        <div class="row" >
        <div class="col-12">
            <h1>` + data.title + `</h1>
            <div>Published: ` + data.created + `</div>
            <hr>
            `+header_image(data.header_image)+`
            <div id="main">` + data.body + `</div>
        </div>
        </div>
    `;
        $('#xanDisplay > div.container').append(individual);

    });
}

function getList(response) {
    $('.dt-list').html('<table id="blogItems" class="display" style="width:100%"></table>');
    $('#blogItems').DataTable({
        responsive: true,
        data: response,
        columnDefs: [
            { responsivePriority: -1, targets: 0 },
            { responsivePriority: 10001, targets: 1 },          
            {
                targets: 0,
                data: 'teaser_image',
                render: function(data, type, row, meta) {
                    return '<img src="https://api.xdm.io' + data + '">';
                }
            },
            {
                targets: 1,
                data: 'title',
                render: function(data, type, row, meta) {
                    var href;
                    host == 'dangibsonme.lndo.site' ? href = '/404.html?path=' + row['path'] : href = row['path'];
                    return `
                    <h2><a href="` + href + `">` + data + `</a></h2>
                    <div class="font-monospace">` + row['summary'] + `</div>
                    <div class="fw-lighter"><em>Created: ` + row['created'] + `</em></div>
                `;
                }
            },
        ],
    });
}

var host = window.location.host;
path = window.location.pathname;

var drupalPath = window.location.search.split('=');
drupalPath = drupalPath[1];

path == '/404.html' ? path = drupalPath : false;

var apiUrl = 'https://api.xdm.io/api/dgme-posts/v1';
path == '/404.html' || path != '/' ? apiUrl = "https://api.xdm.io/api/dgme-posts/v1/post?path=" + path : false;

var settings = {
    url: apiUrl,
    method: "GET",
    timeout: 0,
    headers: {
        Authorization: "Basic ZGdtZTpkZ21l"
    },
};

$.ajax(settings).done(function(response) {
    path == '/' ? getList(response) : getIndiv(response);
});