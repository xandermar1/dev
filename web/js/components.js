function getFooter() {
    var year = new Date().getFullYear();
    return `
        <footer class="bg-light pb-3 text-center">
            <hr>
            <copyright>&copy;` + year + ` DanGibson.me</copyright>
        </footer>    
    `;
}

$('component').each(function() {
    var component = $(this).data('id');
    component === 'footer' ? $(this).html(getFooter()) : $(this).load('/components/' + component + '.txt');
});

function YoutubePlaylistItems(id) {
    var list = '<ul>';
    $.ajax({
        url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&playlistId=" + id + "&maxResults=50&key=AIzaSyD9lZ5dvM6N200mP71L7xfwMRqvNHbPz_4",
        method: "GET",
        timeout: 0,
        async: false,        
    }).done(function(response) {
        for (let i = 0; i < response.items.length; i++) {
            list += '<li><a href="/youtube?id=' + response.items[i].contentDetails.videoId + '">' + response.items[i].snippet.title + '</a></li>';
        }
    });
    list += '</ul>';
    return list;
}

function youtubeResponse(response) {
    $('youtube').html('<h1>YouTube Playlists</h1><hr>');
    for (let i = 0; i < response.items.length; i++) {
        var title = response.items[i].snippet.title;
        var id = response.items[i].id;
        var playlistItems = YoutubePlaylistItems(id);
        title == 'GoPro Karma Flights' ? true : $('youtube').append('<h2>' + title + '</h2>' + playlistItems);
    }
}

function karmaResponse(response) {
    $('karma').html('<h1>The Karma Flight Playlist</h1><hr>');
    $('karma').append('<ul>');
    for (let i = 0; i < response.items.length; i++) {
        var title = response.items[i].snippet.title;
        var id = response.items[i].contentDetails.videoId;
        title == 'Private video' ? true : $('karma').append('<li><a href="/karma?id=' + id + '">' + title + '</a></lih2>');
    }
    $('karma').append('</ul>');
}

function video(vidId, page) {

    $.ajax({
        url: "https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails&id=" + vidId + "&key=AIzaSyD9lZ5dvM6N200mP71L7xfwMRqvNHbPz_4",
        method: "GET",
        timeout: 0,        
    }).done(function(response) {
        var id = response.items[0].id;
        var title = response.items[0].snippet.title;
        var desc = response.items[0].snippet.description;
        var publishedAt = response.items[0].snippet.publishedAt;
        page == 'youtube' ? pageLinkText = 'YouTube Playlists' : false;
        page == 'karma' ? pageLinkText = 'The Karma Flight Playlist' : false;
        var ret = `
            <div class="text-center">
                <h2>` + title + `</h2>
                <div>` + desc + `</div>
                <div>Published: ` + publishedAt + `</div>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/` + id + `" 
                    class="embed-responsive-item" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; 
                    clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
                    allowfullscreen></iframe>  
                </div> 
                <div><a href="/` + page + `">Back to ` + pageLinkText + `</a></div>
            </div>
        `;
        $(page).html(ret);
    });
}

function youtubePage(){
    $.ajax({
        url: "https://www.googleapis.com/youtube/v3/playlists?part=snippet%2CcontentDetails&channelId=UCdGutuh9KwKQ0EmOLVwIJ4A&maxResults=25&key=AIzaSyD9lZ5dvM6N200mP71L7xfwMRqvNHbPz_4",
        method: "GET",
        timeout: 0,    
    }).done(function(response) {
        $('youtube').html('');
        const params = new URL(location.href).searchParams;
        const vidId = params.get('id');
        vidId ? video(vidId, 'youtube') : youtubeResponse(response);
    });    
}

function karmaPage(){
    $.ajax({
        url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&playlistId=PLcwfTujo03do3X7VGxO1xTvjAfj4r3CzZ&maxResults=50&key=AIzaSyD9lZ5dvM6N200mP71L7xfwMRqvNHbPz_4",
        method: "GET",
        timeout: 0,
    }).done(function(response) {
        $('karma').html('');
        const params = new URL(location.href).searchParams;
        const vidId = params.get('id');
        vidId ? video(vidId, 'karma') : karmaResponse(response);
    });
}

var p = window.location.pathname;
p=p.substring(1);

// load page
switch(p) {
  case 'youtube':
    youtubePage();
    break;
  case 'karma':
    karmaPage();
    break;
}