<header class="site-header mb-3" role="banner">
        <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
            <div class="container-fluid">
                <div class="navbar-brand"><a href="/"><img src="/assets/img/logo.png" class="img-fluid"></a></div>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/about" style="cursor:pointer;"><img
                                    src="/assets/img/about-me.png" class="img-fluid"></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/karma" style="cursor:pointer;"><img
                                    src="/assets/img/karma.png" class="img-fluid"></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/youtube" style="cursor:pointer;"><img
                                    src="/assets/img/my-youtube.png" class="img-fluid"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>